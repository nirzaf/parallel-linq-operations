﻿using System;
using System.Linq;

namespace DemoParallel
{
    public static class CustomAggregation
    {
        public static void Aggregation()
        {
            //Parallel Sum
            var sum = ParallelEnumerable.Range(1, 1000).Aggregate(0, (partialSum, i) => partialSum += i, (total, subtotal) => total += subtotal, i => i);
            Console.WriteLine($"Sum = {sum}");
        }
    }
}