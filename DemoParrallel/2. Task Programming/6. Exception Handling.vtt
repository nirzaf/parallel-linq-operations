WEBVTT

00:02.210 --> 00:05.890
It's no secret that different tasks can throw different types of exceptions.

00:05.900 --> 00:11.610
And at the moment the situation is such that if we don't handle an exception in a separate tasks there

00:11.610 --> 00:13.230
is going to be ignored completely.

00:13.370 --> 00:14.020
Here's what I mean.

00:14.030 --> 00:20.240
Let's suppose that I make a task class a task that factory does start new and I make a task which simply

00:20.240 --> 00:26.610
throws let's say an invalid operation exception invalid operation exception.

00:26.610 --> 00:26.910
There we go.

00:26.930 --> 00:28.040
So I have this task.

00:28.040 --> 00:33.350
It's already running but if I execute the program it looks as though things are in good order.

00:33.350 --> 00:36.100
Main program is done and nothing bad seems to be happening.

00:36.110 --> 00:38.180
But we did in fact throw an exception.

00:38.210 --> 00:42.320
So the question is how can we actually track that those exceptions have been thrown.

00:42.530 --> 00:46.550
Just for illustration purposes let me give a variable to this task.

00:46.550 --> 00:49.010
Let's call T and let's make another task.

00:49.010 --> 00:54.080
I'll just copy this this entire thing so we'll have another task which throws a different exception.

00:54.080 --> 00:58.520
So we're going to have T-2 which throws an access violation exception.

00:58.550 --> 01:05.330
So in both of these cases I might want to have some message like in this case I will say can't do this

01:05.780 --> 01:07.870
and I'll provide the source to BT.

01:07.910 --> 01:10.100
So we know where the exception comes from.

01:10.130 --> 01:20.930
And similarly here I will say can't access this and I'll provide the source to BT to now once again

01:21.200 --> 01:24.060
if I just execute my program everything is fine.

01:24.090 --> 01:28.520
You know it doesn't look as though anybody's doing anything which is go through the main program and

01:28.520 --> 01:32.090
the side threads Well they did something but nobody knows what.

01:32.120 --> 01:34.650
But as soon as I do task Don't wait.

01:34.650 --> 01:38.110
So what happens is we observe those exceptions.

01:38.150 --> 01:43.070
So we take TNT too and we actually observe those exceptions and if I run the program now will guess

01:43.070 --> 01:49.540
what I get the program crashes basically and then gives me the stack of the unhandled stuff that should

01:49.600 --> 01:50.500
have been handled.

01:50.660 --> 01:51.960
So the question is how do we handle it.

01:51.980 --> 01:56.090
Well we put a try catch around the computer a try catch on the way to all.

01:56.420 --> 02:00.310
And here we can catch a special type exception called an aggregate exception.

02:00.500 --> 02:05.180
So this is an exception type that specifically designed for its EPO and the idea is that you collect

02:05.510 --> 02:10.940
all the exceptions from all the tasks that you are working on and you put them into a single exception

02:11.210 --> 02:11.900
essentially.

02:12.080 --> 02:19.160
So one of the ways of handling this exception is I can go through through each of the inner exceptions

02:20.540 --> 02:27.680
because a has interceptions and I can actually report them them so I can for example write line some

02:27.680 --> 02:35.270
information I can say that exception of a particular type was acquired from that component specified

02:35.270 --> 02:36.590
by either source.

02:36.590 --> 02:42.040
So now if I execute the program no longer crashes and I get my output so I get an exception called invalid

02:42.170 --> 02:46.700
operation exception from C and I get an access violation exception from C too.

02:46.700 --> 02:51.320
So this is great and this is how you can handle lots of exceptions at the same time.

02:51.320 --> 02:56.900
Now what happens with the exception is you have an even more powerful mechanism for handling on the

02:56.900 --> 03:02.150
part of the aggregate exception and propagating the rest of the aggregate exception up the hierarchy

03:02.150 --> 03:02.920
so to speak.

03:03.110 --> 03:08.120
So let's say call the code that I've written here and I put it into a separate function so that's extract

03:08.120 --> 03:10.820
method and call this demo.

03:11.080 --> 03:12.530
So test actually.

03:12.560 --> 03:14.390
So we have this function called test.

03:14.390 --> 03:20.300
Now what I can do is I can see for example that I'm only prepared to handle the invalid operation exception

03:20.660 --> 03:23.730
so I can Hirose handle.

03:24.380 --> 03:26.290
And this takes a predicate.

03:26.330 --> 03:28.340
So it takes something which returns a boolean.

03:28.340 --> 03:36.920
So I take an exception and I say well if if this exception is in fact an invalid operation exception

03:38.120 --> 03:43.040
then I'm OK so I can write a line.

03:43.030 --> 03:48.600
Invalid operation and I can return true and return true means that I've taken care of this exception

03:49.230 --> 03:53.820
if I haven't then I can return false.

03:53.820 --> 03:55.920
And this means I didn't handle this exception.

03:55.980 --> 03:59.200
So if I now execute this the program will crash once again.

03:59.210 --> 04:05.250
And that's because you can see that the program has crashed with the aggregate exception because even

04:05.250 --> 04:12.420
though it seems as though we caught the entire set of exceptions what we did is we explicitly specify

04:12.420 --> 04:15.780
that we're only prepared to handle the invalid operation exception.

04:15.870 --> 04:20.820
And the other operation exception called propagated up the stack inside and that we are the exception

04:21.120 --> 04:22.570
which means that I can take this.

04:22.580 --> 04:29.430
I can take this invocation of test I can surround it once again with a try catch block where I catch

04:29.430 --> 04:36.370
and aggregate the exception once again calling into to pay me and here what I can do is I can say well

04:36.400 --> 04:36.970
OK.

04:37.120 --> 04:42.790
So we've handled the invalid operation exceptionable but didn't handle the other exceptions.

04:42.790 --> 04:52.450
So here I can for example state that for for each exception in a minor exceptions I can once again print

04:52.450 --> 05:03.310
something out so I can for example say handled elsewhere and gets either what I get type as soon as

05:03.310 --> 05:06.660
I put the dollar sign in here and we can execute this.

05:06.670 --> 05:12.710
And this means that we now handle the invalid operation right next to the task.

05:12.710 --> 05:13.750
That way though.

05:13.960 --> 05:16.310
But the other thing is handled elsewhere.

05:16.360 --> 05:20.270
So this is where we handle the access violation exception.

05:20.290 --> 05:23.830
So this is roughly how you handle exceptions in tasks.
