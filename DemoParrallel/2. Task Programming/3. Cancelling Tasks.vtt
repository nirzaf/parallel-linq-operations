WEBVTT

00:02.180 --> 00:07.730
Tasks get executed on separate threads which means at some point you might want to cancel a particular

00:07.730 --> 00:12.890
task and the task parallelize provides explicit mechanism for doing exactly that.

00:12.890 --> 00:14.650
So let's take a look at how it works.

00:14.660 --> 00:15.960
Let me make it task t.

00:15.980 --> 00:18.570
I'll make you task which basically runs forever.

00:18.800 --> 00:24.680
So I'm going to have a task which does something for Everleigh have a counter by starting at zero and

00:24.680 --> 00:30.000
then while true I'm basically going to output that counter and incremented as well.

00:30.080 --> 00:36.410
So I'll have Konsole don't write line I'll have the counter being incremented and a tab character.

00:36.410 --> 00:40.780
So we're doing this task for ever and ever and you can never stop it.

00:40.790 --> 00:46.070
So once I do T-Dog start the task will run to infinity.

00:46.070 --> 00:51.180
So the question is how can I actually cancel the execution of this task how can I stop it.

00:51.410 --> 00:56.140
OK so in order to support cancellation the task for library gives you two things.

00:56.240 --> 01:00.060
It gives you a cancellation token source and cancellation tokens.

01:00.110 --> 01:05.640
So the first of those is a cancellation token sourceless make one is a cancellation token source.

01:05.690 --> 01:11.000
Now from this you get a so-called cancellation token and the token is exactly that which you provide

01:11.270 --> 01:14.660
into the body of a task when you want to cancel the task later on.

01:14.660 --> 01:20.870
So essentially when we say something like Marteau can call CPSU token and it's exactly this token that

01:20.870 --> 01:26.780
is provided here for example so we provide a token you can see that it's in the overloads there is a

01:26.780 --> 01:29.440
cancellation token that's exactly what we're providing.

01:29.660 --> 01:35.630
So now that we've provided a cancellation token we can call CTX that council if you want to actually

01:35.630 --> 01:37.040
cancel the execution.

01:37.040 --> 01:43.370
So here I'll do a console adult read key so we pause on the inputs and whenever you press a particular

01:43.370 --> 01:48.050
key We're going to cancel the whole thing cancel essentially the task.

01:48.050 --> 01:53.030
However you have to understand that in TPL cancellation is called pro-the which means the task that

01:53.030 --> 01:59.240
you've made has to be explicitly aware of the fact that is cancelable and it has to perform certain

01:59.240 --> 02:01.450
actions to actually cancel itself.

02:01.460 --> 02:05.810
So there are two ways of cancelling your execution.

02:05.810 --> 02:07.850
One of these is kind of soft failure.

02:07.850 --> 02:09.290
Remember we're in the wild.

02:09.290 --> 02:13.340
True love we can always do a brake or a return and then we're out.

02:13.430 --> 02:14.750
We're done with a task.

02:14.780 --> 02:16.550
So that's one of the ways of doing it.

02:16.550 --> 02:22.880
What I can do here is I can look whether cancellation has been requested on this particular topic so

02:22.880 --> 02:30.420
I can essentially say well if token Daut is cancellation requested then somebody wants to cancel execution.

02:30.430 --> 02:32.340
In this case I can just do a break.

02:32.420 --> 02:36.870
Otherwise yes I'm going to continue outputting those characters.

02:37.070 --> 02:39.290
So that's one of the ways of doing it.

02:39.290 --> 02:42.420
And let's actually take a look at how it works let me execute the thing.

02:42.440 --> 02:45.200
So as you can see we get a never ending stream of numbers.

02:45.200 --> 02:52.850
However as soon as I press a letter for example we have the execution stop because essentially I called

02:53.180 --> 02:59.540
CPS thought counsel on that cancellation tokens so we input a character and then CPS that canso essentially

02:59.750 --> 03:02.340
counsels the whole thing.

03:02.390 --> 03:06.450
So tokened is cancellation requests that get said to true and we breakout.

03:06.470 --> 03:13.850
So this is the kind of soft exit out overtasked there is a more canonical way of getting out of the

03:13.850 --> 03:19.520
whole thing which is to use exceptions instead and thats something that PPO actually recommends you

03:19.520 --> 03:20.190
do.

03:20.480 --> 03:22.190
So there are two ways of doing it.

03:22.220 --> 03:25.080
One of those ways is as follows.

03:25.220 --> 03:31.640
Instead of just breaking out the kind of soft exit you are you do you throw an exception and specifically

03:31.640 --> 03:36.160
the exception you throw is an operation cancel the exception.

03:36.200 --> 03:40.330
So thats a canonical way of actually jumping out of the whole thing.

03:40.340 --> 03:44.090
And no its not going to really cause exception on the high level.

03:44.090 --> 03:50.060
So let me actually execute this and essentially as soon as I press something the task has canceled them

03:50.060 --> 03:52.070
were essentially done there is no problem here.

03:52.070 --> 03:57.260
Its not like we just threw an exception which got propagated all the way up because we were expecting

03:57.260 --> 03:58.440
this kind of exception.

03:58.550 --> 04:00.890
Unlike some other sort of exception.

04:00.950 --> 04:03.320
So thats one of the ways of canceling tasks.

04:03.320 --> 04:10.970
Another way is when this line and this line get merged together thats just a syntactic shortcut that

04:11.210 --> 04:17.510
TPL gives us so instead of those two lines the check and the invocation what you can do is you can say

04:17.540 --> 04:22.430
token Daut through if cancellation requests it is basically a combination of those two things.

04:22.430 --> 04:27.380
First of all it checks whether somebody requests the cancellation and if somebody did request cancellation

04:27.380 --> 04:28.760
then it throws that exception.

04:28.760 --> 04:31.150
The end result is absolutely the same.

04:31.160 --> 04:36.660
And this is the canonical way of stopping the execution of a task.

04:36.680 --> 04:43.400
Now there is a difference in terms of the status that you're going to see if you take a look at the

04:43.400 --> 04:50.330
status of the task after is being executed because if you throw then yes the tasks knows that it is

04:50.330 --> 04:51.050
being canceled.

04:51.050 --> 04:55.460
How if you don't throw the exception the task isn't going to know that it's being canceled it's just

04:55.460 --> 04:57.690
going to tell you that it completed successfully.

04:57.830 --> 05:02.660
So the question is whether or not you want to actually be notified of the fact that some of the counsel

05:02.840 --> 05:03.580
the task.

05:03.590 --> 05:05.140
It's really a design decision.

05:05.150 --> 05:09.890
However this is the canonical way this is the way that C.P.R. recommends you do this because then you

05:09.890 --> 05:15.020
have a record of the fact that somebody canceled this particular task and also when you have several

05:15.020 --> 05:21.290
tasks tied to a single token you can better figure out what happened whether the task completed successfully

05:21.290 --> 05:24.270
or whether somebody generally counseled.

05:24.630 --> 05:29.440
OK so one question that we might want to answer is how can we monitor when a task is canceled.

05:29.450 --> 05:33.830
How can we be notified about it and once again there are different ways of doing exactly that.

05:33.830 --> 05:39.180
So one of those ways is you can basically subscribe to an event that's available right on the tokens

05:39.180 --> 05:43.970
so you can see token register and then you can provide some sort of function which actually handles

05:43.970 --> 05:49.580
the situation when somebody wants to counsel on the token source so in this case I'll console right

05:49.590 --> 05:53.420
line and will say cancellation has been requested.

05:53.810 --> 05:59.390
So that's essentially a function that is going to be executed as soon as somebody does CPS the counsel.

05:59.390 --> 06:07.370
So that's one of the ways of doing it and other way which is also very interesting is to basically wait

06:07.430 --> 06:10.980
on the cancellation tokens wait Hanno it's a bit more complicated.

06:11.000 --> 06:16.620
But I do want to show it to you so let me actually write this down so I'm going to spin up another task.

06:16.640 --> 06:21.600
Task that task that factory does start new.

06:22.070 --> 06:26.110
And this is going to be a bit confusing but I'll go slow and actually explain what's going on.

06:26.240 --> 06:32.000
So one of those synchronization primitives that we'll talk about later in this course is called a wait

06:32.010 --> 06:32.550
handover.

06:32.570 --> 06:39.670
And it just so happens that a token that you're using to cancel tasks is equipped with such a way handle

06:39.700 --> 06:45.380
and essentially the idea for this way to handle is that you can wait on something else to occur it's

06:45.380 --> 06:50.990
kind of like an event it's kind of equivalent to having a subscription to an event here because essentially

06:50.990 --> 06:56.660
when you call register you're subscribing to an event that happens here the idea is that you take the

06:56.660 --> 07:00.640
tokens way Tandou explicitly and you say I'm going to wait.

07:00.770 --> 07:07.970
I'm going to wait on this handle until somebody cancels the token until somebody actually invokes the

07:07.970 --> 07:08.650
cancellation.

07:08.660 --> 07:15.530
So as soon as cancellation is invoked this function call which is actually blocking will release and

07:15.530 --> 07:17.750
will allow you to do whatever it is you want to do.

07:17.750 --> 07:23.570
So in this case I can once again console right line and I can say that the way handle has been released

07:24.500 --> 07:27.820
and therefore a cancellation was requested.

07:28.130 --> 07:32.480
So let's take a look at what happens when we execute all of these.

07:32.480 --> 07:35.820
OK so I'm going to press a key and you can see the output here.

07:35.810 --> 07:38.510
So we have the output cancellation has been requested.

07:38.510 --> 07:43.870
That's from that additional call that we made the explicit kind of notification.

07:43.970 --> 07:48.050
And then here is the way to handle a call Notice they don't have to be any sort of sequence.

07:48.050 --> 07:51.230
They can happen concurrently so to speak.

07:51.230 --> 07:57.080
So that's two of the ways in which you can actually monitor that somebody wants to essentially cancel

07:57.080 --> 07:58.310
the task.

07:58.310 --> 08:01.450
The last thing I'm going to show is a bit more complicated.

08:01.460 --> 08:04.110
It's to do with composite cancellation tokens.

08:04.130 --> 08:08.060
So let me get rid of everything that we've written so far and I will show you how that works.

08:08.060 --> 08:12.740
So essentially the idea is that you can have several cancellation tokens sources for example you might

08:12.740 --> 08:16.330
have a token source for cancellations which were planned.

08:16.330 --> 08:23.240
So you see planned equals new cancellation token source but you might also have a cancellation to open

08:23.240 --> 08:25.170
source for preventative measures.

08:25.430 --> 08:27.830
Preventative Nusi Yes.

08:28.010 --> 08:33.100
And you might also have a cancellation token SOS for emergencies like run out of memory for example

08:33.110 --> 08:36.470
so say emergency equals and use.

08:36.490 --> 08:36.970
Yes.

08:37.120 --> 08:37.370
OK.

08:37.370 --> 08:43.010
So we have different cancellation two sources what we can do is we can now specify a token service which

08:43.010 --> 08:48.680
reacts to any of those three token services being triggered essentially a kind of paranoid mode so I

08:48.680 --> 08:51.290
can say more paranoid he calls.

08:51.290 --> 08:55.540
And then I can do cancelation token source doth create link source.

08:55.550 --> 09:02.780
So essentially what this does is you can take the tokens from the plant preventative and emergency token

09:02.780 --> 09:08.720
sources and you can feed them into a new tokens or so paranoid is going to be a consolation to consumers

09:09.050 --> 09:15.620
which essentially if you get a token from paranoid that token will cause cancellation when you trigger

09:15.860 --> 09:19.910
when you call canso on either planned or preventative or emergency.

09:19.910 --> 09:29.000
So here you specify the tokens you can say plan a token preventative token and emergency token and you've

09:29.000 --> 09:31.430
now created this kind of linked situation.

09:31.430 --> 09:31.870
OK.

09:31.970 --> 09:34.490
So how do we actually do all of this.

09:34.490 --> 09:39.400
Let me make a new task so I'll once again test our factory don't start new.

09:39.860 --> 09:44.870
Let's make a lambda here a saw have some counter and then.

09:45.010 --> 09:45.380
Wow.

09:45.380 --> 09:45.910
True.

09:45.950 --> 09:53.060
I'm going to basically console adult right Linus before so we'll write line with a dollar we'll write

09:53.060 --> 09:53.590
a line.

09:53.660 --> 09:55.560
Plus plus followed by tab.

09:55.880 --> 10:02.280
OK so now what I'm going to do is I can actually check whether Consolacion has been requested the token

10:02.520 --> 10:11.530
from this paranoid merger of the cancellation took and so essentially I can say paranoid can dot throw

10:11.700 --> 10:18.540
cancellation was requested so I'll put this I'll put asleep in here just so we don't get a massive strolling

10:18.570 --> 10:20.990
stream of numbers that's annoying.

10:21.150 --> 10:25.500
Sleep for one hundred for example 1 7 1 0 0 0 0.

10:25.680 --> 10:28.440
OK so here I'm executing this task.

10:28.440 --> 10:31.140
I'm providing paranoid token.

10:31.170 --> 10:35.340
So that's the token from the linked token source.

10:35.340 --> 10:36.640
Now what does this mean.

10:36.680 --> 10:37.670
What do we do all of this.

10:37.680 --> 10:44.160
Well essentially what this implies is you can now request cancellation on either planned or preventative

10:44.430 --> 10:46.380
or emergency token sources.

10:46.380 --> 10:52.500
And because we have a token from a linked token source that will stop our execution so it doesn't matter

10:53.010 --> 10:54.030
what I do here.

10:54.030 --> 11:02.010
For example I can see emergency or Council emergency council and this would actually cause paranoid

11:02.310 --> 11:09.510
token to cause the stoppage of this because it's linked because essentially paranoid is linked to both

11:09.510 --> 11:11.400
plant and preventative and emergency.

11:11.400 --> 11:17.550
So if I had here planned dot canso or emergency council it would still stop the thread.

11:17.580 --> 11:19.580
So let's actually see how it works out.

11:19.590 --> 11:25.770
Once again put a console read key here and we can run this and see what we get.

11:26.910 --> 11:32.970
So here we are slowly filling a stream of numbers as soon as I press return you can see the program

11:32.970 --> 11:38.100
is done because we have essentially cancelled and it doesn't matter which of these cancellation token

11:38.100 --> 11:43.920
sources we cancel because they were all linked to the paranoid token which is exactly what we used in

11:43.920 --> 11:46.380
the creation of the task.

11:46.710 --> 11:48.420
And this is how you counsel tasks.
