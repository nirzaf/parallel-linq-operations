WEBVTT

00:02.210 --> 00:06.750
We aren't now going to discuss something called a reader right or Lawk.

00:06.770 --> 00:14.030
So the idea of a reader writer lock is it's a smarter kind of lock if you think about the operations

00:14.030 --> 00:21.260
that you would put a lock on then typically those operations would involve either reading the data or

00:21.320 --> 00:22.910
changing the data somehow.

00:22.910 --> 00:29.330
And what reader write the law does is it basically lets you read the data from several locations concurrently

00:29.660 --> 00:31.330
or several threads rather.

00:31.340 --> 00:38.570
But it controls the writing process by sort of locking on the writing part and not letting several different

00:38.570 --> 00:39.000
threads.

00:39.000 --> 00:44.460
Right at the same time which is a safe way of going about working with data.

00:44.480 --> 00:47.820
So let's take a look at how we can set this whole thing up.

00:47.900 --> 00:54.380
Get rid of the example that we had previously and we'll build something new so the reader writer lock

00:54.380 --> 00:59.080
is made using the reader writer lock slim class.

00:59.090 --> 01:06.590
There was also an older reader writer lock which isn't as good a reader writer lock slim that some more

01:06.590 --> 01:10.910
modern variety and it is better and it is what you should be using.

01:10.970 --> 01:18.740
So I'll make this call padlock so I'll make a new read or write a long slim you'll notice that one of

01:18.740 --> 01:25.100
the arguments here is a lock recursion policy and yes read the right Alex lim does in fact support lock

01:25.160 --> 01:28.330
recursion if you want to go in this direction.

01:28.340 --> 01:33.860
So let's suppose I have an integer variable called X with an initial value of 0 and I'm good to Big

01:33.860 --> 01:36.910
Lots of tasks which are going to change this variable.

01:37.100 --> 01:48.170
So once again we'll have a list of tasks bar tasks a new list of task like so and then I'll make a loop

01:48.170 --> 01:56.540
so I'll have 10 iterations of the loop and inside those iterations I'll add a task and this task once

01:56.540 --> 01:59.780
again I'll do a task the factory does start new.

02:00.170 --> 02:02.760
This task is going to do the following.

02:02.840 --> 02:09.430
It's going to basically output the fact that we entered some sort of read log.

02:09.430 --> 02:12.110
So we're going to read the variable First of all.

02:12.110 --> 02:20.310
So we want to write line what I want to do is I want to simply write the value of x.

02:20.330 --> 02:25.070
That's simple enough but I need to control access to this variable because at some point I might be

02:25.070 --> 02:26.370
writing to it.

02:26.390 --> 02:31.200
So the way this is done using the reader i.e. the Loxley is as follows.

02:31.220 --> 02:35.600
You basically say padlock Daut enter read lock.

02:35.840 --> 02:41.840
So to read the variable you enter the read lock and then you can read it and output it to the console.

02:41.840 --> 02:49.840
So here I can say and to read lock and x is equal to the value of x like so.

02:50.060 --> 02:57.650
OK so then I'm just going to sleep for a moment let's say five seconds and then I need to accept the

02:57.650 --> 02:58.100
read log.

02:58.100 --> 03:04.550
So I have to say explicitly pack padlocked not exit read lock to sort of give up the law and then let's

03:04.580 --> 03:06.890
write line once again.

03:06.890 --> 03:15.830
So here I'm going to say X it did read like an x is equal to the value of x like so.

03:15.830 --> 03:18.340
So this is how you would control the reading.

03:18.350 --> 03:22.010
We're not doing any changes to X at the moment.

03:22.010 --> 03:27.050
So essentially at the moment we've just made these ten tasks I'm going to wait on them so I'll have

03:27.050 --> 03:32.660
a try catch where I basically do a task that way at all.

03:32.870 --> 03:35.930
How do tasks start to array as we did before.

03:36.050 --> 03:40.120
And in the case of exceptions I say we get an aggregate exception.

03:40.120 --> 03:45.140
I'm just going to handle it by writing it to the command line so I'll say.

03:45.840 --> 03:54.260
Well as soon as I give it a name I'll call a dot Hanzo and take the exception and simply cancel the

03:54.320 --> 03:55.390
right line.

03:55.460 --> 04:01.490
The exception and return true meaning we've handled it and then what I'm going to do is I'm not going

04:01.490 --> 04:06.310
to have an input loop where I'll be actually trying to change the value of x.

04:06.320 --> 04:15.450
So while true I'm basically going to read the key but once I'm done reading the key I'm going to enter

04:15.450 --> 04:16.600
a write lock.

04:16.710 --> 04:22.290
So in this particular instance not only do I want to just read the value of x I want to write it as

04:22.290 --> 04:22.600
well.

04:22.620 --> 04:27.390
And in this case I'll do padlock thought enter right lock.

04:28.410 --> 04:32.670
And in fact I'll notify about it so I'll say right.

04:32.680 --> 04:33.690
Lock acquired

04:36.350 --> 04:41.330
then I might change the x value to some random number.

04:41.330 --> 04:44.650
Let me actually just make a random generator

04:47.510 --> 04:49.980
have it as static as well.

04:50.030 --> 04:53.600
So down here I might change x to some new value.

04:53.630 --> 05:04.980
So the new value equals random dot next 10 and then x equals new value I'll also write a line about

05:04.980 --> 05:09.950
it so I'll say set X equals the value of x.

05:09.960 --> 05:14.860
Obviously with interpellation and then I will release.

05:15.000 --> 05:26.070
So I will exit the right lock padlock exit right lock and then write right line that the lock has been

05:26.070 --> 05:26.960
released right.

05:26.970 --> 05:29.140
Lock released.

05:29.160 --> 05:29.890
There we go.

05:30.090 --> 05:35.460
OK so I have the main thread doing the modifying and another other thread doing just the reading.

05:35.640 --> 05:39.740
Let's see what happens when we actually go ahead and execute this.

05:39.750 --> 05:45.510
So as you can see we've entered lots of read locks at the same time because all those 10 threads were

05:46.020 --> 05:52.500
spawned at the same time and they both entered and then they both accepted and when I requested by right

05:52.890 --> 05:59.760
that managed to appear only at the end because obviously I asked for it a lot later than some of the

05:59.760 --> 06:07.840
read locks if I tried doing it again and just try to outrun the creation of the read box of press it

06:07.860 --> 06:13.230
like this I don't think I'm going to be able to because they get created very quickly inside that for.

06:13.260 --> 06:20.160
Before I get the chance to do a real key even though this is all obviously happening concurrently.

06:20.220 --> 06:24.880
So we've entered all those reed locks and there they have been processed.

06:24.880 --> 06:28.290
Then subsequently when the time comes I'll get my right lock.

06:28.290 --> 06:35.100
You'll see I get actually a lot of them where I get the right lock I set the X and I kind of release

06:35.100 --> 06:36.150
it as well.

06:36.240 --> 06:38.730
So what can we do with this reader writer lock.

06:38.730 --> 06:41.250
Well first of all let's talk about recursion.

06:41.250 --> 06:47.520
Now remember recursion is when you try to take the read lock twice so let me just if I just copy this

06:47.790 --> 06:55.260
and then copy this as well then under the normal conditions under the default constructor argument I

06:55.260 --> 06:56.650
should not be able to do it.

06:56.850 --> 06:58.380
I should just get lots of exceptions.

06:58.380 --> 07:05.400
So for each of the threads or each of the tasks in fact that we created we got a lock recursion exception

07:05.400 --> 07:09.360
where the recursive read lock acquisition is not allowed in this mode.

07:09.360 --> 07:16.200
So what we can do is we can change the mode and we can basically go up here and provide an argument

07:16.200 --> 07:18.300
to the reader right Loxley constructor.

07:18.300 --> 07:21.460
There is a support for recursion constructor.

07:21.630 --> 07:26.510
So now if I do this then you'll notice the program is once again functioning.

07:26.520 --> 07:26.930
OK.

07:26.940 --> 07:33.750
We enter our read locks we perform the five second way and then we exit those read locks and then I

07:33.750 --> 07:36.820
can sort of before my rights as usual.

07:36.840 --> 07:42.720
So that basically shows you that if you needed lock Rickerson support is there although once again I

07:42.720 --> 07:48.390
don't recommend it because it's very easy to get lost and confused when using lock recursion but sometimes

07:48.390 --> 07:49.540
it is necessary.

07:49.560 --> 07:50.310
So that's one thing.

07:50.310 --> 07:56.010
Another thing is something called an upgradable we'd like to see what would happen if we try to take

07:56.100 --> 08:01.470
a write off for some reason imagine we only wanted to read the variable but we realized that we really

08:01.470 --> 08:02.530
need to change it.

08:02.610 --> 08:09.210
If you just do padlocked or enter right lock here what you're going to see is that you get tons of exceptions

08:09.210 --> 08:15.520
because you are not allowed to do this essentially basically tells you this pattern is prone to deadlocks.

08:15.540 --> 08:20.640
Please ensure that Reed locks are released before taking write lock if an upgrade is necessary use an

08:20.640 --> 08:21.660
upgrade lock.

08:21.660 --> 08:24.890
So that's what we're going to do again to use an upgrade lock in place.

08:24.930 --> 08:30.240
So if you want to use an upgrade lock then instead of entering just an ordinary read lock you have to

08:30.630 --> 08:34.750
do it differently you have to say that you want an upgradable read lock.

08:34.780 --> 08:41.820
So essentially you say padlock that enter upgradeable read lock like so and of course when you exit

08:42.210 --> 08:50.250
you have to say you have to basically say exit upgradeable read lock like so and if you do want to write

08:50.250 --> 08:58.590
like for example for every even thread for every even tasking our counter Let's suppose I do want to

08:58.590 --> 09:08.080
change the value of x what I can do is I can promote I can upgrade this lock to the right lock.

09:08.190 --> 09:16.230
And this is just done by by using the right lock like so then I can change the value of x and then I

09:16.230 --> 09:19.050
can exit right here.

09:19.110 --> 09:26.400
So if I execute this well as you can see I'm entering my read lock I have X equals to 123 then I take

09:26.400 --> 09:31.740
the five second pause and only then can I do the next one which once again comes with a five second

09:31.740 --> 09:33.680
pause and so on and so forth.

09:33.690 --> 09:36.120
So this is how the whole process works.

09:36.120 --> 09:38.290
If you are using upgradable locks.

09:38.580 --> 09:45.900
So this is all I wanted to say in respect to the reader either or specifically this slim variety which

09:45.900 --> 09:51.510
is somewhat better than the ordinary variety it's essentially a more flexible lock where you can separate

09:51.510 --> 09:54.750
the reads from the rights and be very explicit about it.
